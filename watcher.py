from loguru import logger
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from path import Path
from plumbum.cmd import pandoc, rsync
import time

reveal = pandoc[
    "-s", "-t", "revealjs", "--mathjax", "--css", "custom.css", "--slide-level", "2"
]
beamer = pandoc["-s", "-t", "beamer"]
sync_web = rsync["-av", "./http/", "pataclet:/srv/http/slides/wavecotedazur/"]

class MarkdownHandler(FileSystemEventHandler):
    def on_modified(self, event):
        modified_file = Path(event.src_path)
        if modified_file.ext in [".css", ".md"]:
            logger.info("File %s modified" % modified_file)
            try:
                logger.info("Writting reveal.js slides.")
                reveal(modified_file, "-o", f"http/index.html")
                logger.info("Done.")
            except Exception:
                logger.exception("Unable to write reveal.js slides.")
            try:
                logger.info("rsync to web.")
                sync_web()
                logger.info("Done.")
            except Exception:
                logger.exception("Unable rsync to web.")


def IOLoop(observer):
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        # Ctrl + C arrête tout
        observer.stop()
    # on attend que tous les threads se terminent proprement
    observer.join()


if __name__ == "__main__":
    observer = Observer()
    observer.schedule(MarkdownHandler(), path="./")
    observer.start()
    IOLoop(observer)

