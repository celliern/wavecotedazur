---
title: scikit-fdiff
author: Nicolas Cellier (Université Savoie Mont-Blanc)
date: June 04, 2019
---

## Overview


High performance differential equations solver written in **python** that aim to help model prototyping

::: incremental

- Solving by *line method*
- Spatial discretisation via **finite differences**
- Access to ODE (ordinary differential equations) solver **explicit** as well as **implicit**
- Multi-dimension coupled problems

:::

# Example: Two dimensions acoustic wave propagation

## Implementation

```python
bc = defaultdict(lambda: ("dirichlet", "dirichlet"))

model = Model(
    [
        "-upwind(1/rho, P, x, 1)",
        "-upwind(1/rho, P, y, 1)",
        "upwind(-1/k, Vx, x, 1) + upwind(-1/k, Vy, y, 1) + q",
    ],
    ["Vx(x, y)", "Vy(x, y)", "P(x, y)"],
    parameters=["rho(x, y)", "k(x, y)", "q(x, y)"],
    boundary_conditions=bc,
)
initial_fields = model.Fields(x=..., y=...,
                              P=..., Vx=...,
                              Vy=..., q=...,
                              k=..., rho=...)

simulation = model.init_simulation(model, initial_fields, dt=...)
simulation.run()
```

## Result

Two domains with different sound speed, wall boundary condition.

<br/><br/>
<video width="400" controls autoplay loop>
  <source src="figures/acoustic.mp4" type="video/mp4">
    Your browser does not support the video tag.
</video>

Blue is the negative relative pressure, red the positive one

# Under the hood - The model

## Scikit - fdiff model

::: incremental

- The high-level access to the first part of the discretisation.
- Transforms the spatial derivatives into an ODE system than can be solved by specialised solvers.
- Built on several blocks:
  - PDE system processing
    - Finite differences discretisation
    - Boundary condition domain computation
  - Grid builder with smart vectorisation
  - Numerical routine writing via different backends 

:::

## Finite differences symbolic processing

Using the `sympy` symbolic library: it takes the user input (as string) and return a discrete form using finite differences method.

```python
>>> centered_fdiff = FiniteDifferenceScheme()
>>> centered_fdiff.apply(sympify("U(x).diff(x)"))
```

$$- \frac{U{\left(- dx + x \right)}}{2 dx} + \frac{U{\left(dx + x \right)}}{2 dx}
$$

```python
>>> left_fdiff = FiniteDifferenceScheme(scheme="left")
>>> left_fdiff.apply(sympify("U(x).diff(x)"))
```

$$\frac{3 U{\left(x \right)}}{2 dx} + \frac{U{\left(- 2 dx + x \right)}}{2 dx} - \frac{2 U{\left(- dx + x \right)}}{dx}$$

------------------

## Boundary condition processing

```python
sys = PDESys("-c_x * dxU - c_y * dyU", "U(x, y)",
             parameters=["phi", "c_x", "k", "U_0"],
             boundary_conditions={("U", "x"):
                                  ("dxU - phi",
                                   "dxU - (U - U_0)")})
```

![](figures/domains.svg)

------------------

1. `-c_x*(U[x_idx + 1, 0] - U[x_idx - 1, 0])/(2*dx)`
2. `-c_x*phi`
3. `-c_x*phi`
4. `-(c_x*dy*phi + c_y*(U[0, y_idx + 1] - U[0, y_idx - 1])/2)/dy`
5. `-(c_x*dy*(U[x_idx + 1, y_idx] - U[x_idx - 1, y_idx]) + c_y*dx*(U[x_idx, y_idx + 1] - U[x_idx, y_idx - 1]))/(2*dx*dy)`
6. `c_x*(U_0 - U[N_x - 1, 0])`
7. `c_x*(U_0 - U[N_x - 1, N_y - 1])`
8. `-c_x*(U[x_idx + 1, N_y - 1] - U[x_idx - 1, N_y - 1])/(2*dx)`
9. `(c_x*dy*(U_0 - U[N_x - 1, y_idx]) - c_y*(U[N_x - 1, y_idx + 1] - U[N_x - 1, y_idx - 1])/2)/dy`

------------------

## Smart vectorisation

- Implicit case : need to build the Jacobian matrix 
$$
J(U) =
\begin{bmatrix}
\frac{\partial F_0}{\partial U_0} & \cdots & \frac{\partial F_0}{\partial U_j} & \cdots & \frac{\partial F_0}{\partial U_N} \\
\vdots & \ddots & & &\vdots\\
\frac{\partial F_i}{\partial U_0} &  & \frac{\partial F_i}{\partial U_j} &  & \frac{\partial F_i}{\partial U_N} \\
\vdots & & & \ddots &\vdots\\
\frac{\partial F_N}{\partial U_0} & \cdots & \frac{\partial F_N}{\partial U_j} & \cdots & \frac{\partial F_N}{\partial U_N}
\end{bmatrix}
$$
- $U_i$ order matter

------------------


$$
\begin{cases}
  \frac{\partial u}{\partial t} = \partial_{xx}u \times \partial_{xx}v\\
  \frac{\partial v}{\partial t} = \partial_{xx}u + \partial_{xx}v
\end{cases}
$$

:::::::::::::: {.columns}

::: {.column width=50%"}

$$
U = \begin{bmatrix}
      u_0\\
      \vdots\\
      u_i\\
      \vdots\\
      u_N\\
      v_0\\
      \vdots\\
      v_i\\
      \vdots\\
      v_N
\end{bmatrix}
$$
:::

::: {.column width="50%"}

<img src="figures/naive_vect.png">

::: 

::::::::::::::

------------------


$$
\begin{cases}
  \frac{\partial u}{\partial t} = \partial_{xx}u \times \partial_{xx}v\\
  \frac{\partial v}{\partial t} = \partial_{xx}u + \partial_{xx}v
\end{cases}
$$

:::::::::::::: {.columns}

::: {.column width=50%"}

$$
U = \begin{bmatrix}
      u_0\\
      v_0\\
      \vdots\\
      u_i\\
      v_i\\
      \vdots\\
      u_N\\
      v_N
    \end{bmatrix}
$$
:::

::: {.column width="50%"}

<img src="figures/opti_vect.png">

::: 

::::::::::::::

------------------

$$\begin{cases}
  \frac{\partial u}{\partial t} = \partial_{xx}v \times \partial_{xx}u + \partial_{yy}v \times \partial_{yy}u\\
  \frac{\partial v}{\partial t} = \partial_{xx}u + \partial_{xx}v + \partial_{yy}u + \partial_{yy}v
\end{cases}$$

<img src="figures/2D_comp_vect.png">

------------------

$$\begin{cases}
  \frac{\partial u}{\partial t} = \partial_{xx}v \times \partial_{xx}u + \partial_{yy}v \times \partial_{yy}u\\
  \frac{\partial v}{\partial t} = \partial_{xx}u + \partial_{xx}v + \partial_{yy}u + \partial_{yy}v
\end{cases}$$

<img src="figures/bench_grid.svg">

## Custom backend

::: incremental

- Transforms the symbolic discrete system into a numerical routine.
- Give access to the $F$ vector and the $J$ matrix.
- Built upon `sympy` equation to code tools.
- For now : `numpy` and `numba JIT` backend.
- For implicit cases : **not the bottleneck !** (*Linear system solving is*)

:::

# Temporal scheme and simulation

## Temporal scheme (ODE solver)

A temporal scheme takes advantage from the numerical routines to update the fields
from $t$ to $t + \Delta t$.

```python
model = Model("dxxU", "U(x)")
x = np.linspace(0, 10, 50)
U = x ** 2
initial_fields = model.Fields(x=x, U=U)
scheme = ThetaScheme(model)
t, fields = scheme(t=0, fields=initial_fields, dt=.1)
```

## Available temporal schemes

::: incremental

- all `scipy.integrate.solve_ivp` solver (BDF, RK45...).
- `Theta` schemes
  - $\theta=0$ is the forward Euler scheme.
  - $\theta=1$ is the backward Euler scheme.
  - $\theta=0.5$ is the Crank Nickolson scheme.
- Rosenbrock Wanner schemes (efficient implicit Runge-Kutta schemes).

:::

## Simulation object

::: incremental

- Higher level object, gives access to the full simulation.
- Can return the updated fields at each step or perform all the steps at once.
- Provides useful goodies, such as...
  - Real time display.
  - On-memory or on-disk fields save.
  - Post-processing (post-processed fields being saved / displayed).

:::

. . .

```python
model = Model("-dxU + 0.1 * dxxU", "U(x)", boundary_conditions="periodic")
x = np.linspace(-np.pi, np.pi, 200, endpoint=False)
U = np.cos(x)
initial_fields = model.Fields(x=x, U=U)
simul = model.init_simulation(initial_fields, dt=.1, tmax=20)
container = simul.attach_container()
simul.run()
container.data.U.plot()
```

----------------------

![](figures/simple_diff.png)

# Application cases

## Falling films

### Hydro-only, traveling wave

- Hydrodynamic: 1D asymptotic model

<video width="800" controls autoplay loop id="slowvid1">
  <source src="figures/hydro_per.ogv" type="video/mp4">
    Your browser does not support the video tag.
</video>

## Falling films

### Hydro-only, full plate

- Hydrodynamic: 1D asymptotic model

<video width="800" controls autoplay loop id="slowvid2">
  <source src="figures/hydro_open.ogv" type="video/mp4">
    Your browser does not support the video tag.
</video>

## Falling films

### Full coupling hydrodynamic / Fourier equation

- Hydrodynamic: 1D asymptotic model
- Heat exchange: 2D Fourier equation

<video width="800" controls autoplay loop id="slowvid3">
  <source src="figures/fourier_open.ogv" type="video/mp4">
    Your browser does not support the video tag.
</video>

## Falling films

### Two dimensional falling films

- Hydrodynamic: 2D asymptotic model

<video width="800" controls autoplay loop id="slowvid4">
  <source src="figures/2D_wave.mp4" type="video/mp4">
    Your browser does not support the video tag.
</video>

## Dropplet spreading / splashing

- Finite volumes (hyperbolic part) / finite differences (work in progress)
  - Hyperbolic part explicit
  - Other terms implicit, computed thanks to scikit-fdiff
<br/><br/>
<video width="400" controls autoplay loop id="fastvid1">
  <source src="figures/static_drop.mp4" type="video/mp4">
    Your browser does not support the video tag.
</video>

1D Dropplet splashing, hyperbolic term dealt with finite volumes solver.

# Going further

## New backends

::: incremental

- scikit-fdiff provides access to symbolic discrete piecewise system (one equation per domain).
- Easy implementation of a new backend if needed
- Usefull for explicit computation : **routines are the bottleneck**
- Low level:
  - Fortran, C, C++, Rust ?
- High level:
  - Julia ? JIT languages ?
  - GPU accelerated with CUDA

:::

## Extending with new methods

::: incremental

- Local adaptative time-stepping
- Generalisation of finite volumes / finite differences for mixed hyperbolic models ?
- Continuation for multi-dimensional PDE ?

:::

# Try by yourself !

## Complementary info and links.

- Documented, with tutorial and examples ([scikit-fdiff.readthedocs.io/](http://scikit-fdiff.readthedocs.io/))
- Code source available on gitlab ([celliern/scikit-fdiff/](https://gitlab.com/celliern/scikit-fdiff/)).
  - Modify, adapt the code, contribute !
  - Issue tracker: feature proposals, questions, support requests, or **bug reports**.
- recently published on JOSS. (doi.org/10.21105/joss.01356)
- These slides are available on gitlab as well ([celliern.gitlab.io/wavecotedazur/](https://celliern.gitlab.io/wavecotedazur/))


<script>
  document.querySelector('#slowvid1').playbackRate = 0.5;
  document.querySelector('#slowvid2').playbackRate = 0.5;
  document.querySelector('#slowvid3').playbackRate = 0.5;
  document.querySelector('#slowvid4').playbackRate = 1.5;
  document.querySelector('#fastvid1').playbackRate = 4;
</script>